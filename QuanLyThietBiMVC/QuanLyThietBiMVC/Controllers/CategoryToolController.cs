﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuanLyThietBiMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyThietBiMVC.Controllers
{
    public class CategoryToolController : Controller
    {
        private readonly QuanLyThietBiContext _context;
        public CategoryToolController(QuanLyThietBiContext context)
        {
            _context = context;
        }
        // GET: CategoryToolController
        public async Task<IActionResult> Index()
        {
            var courses = _context.CategoryTools.ToListAsync();
            return View(await courses);
        }

        // GET: CategoryToolController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var course = await _context.CategoryTools.FirstOrDefaultAsync(m => m.CategoryToolId == id);
            if (course == null)
            {
                return NotFound();
            }
            return View(course);
        }

        // GET: CategoryToolController/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CategoryToolController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CategoryToolName")] CategoryTool categoryTool)
        {
            if (ModelState.IsValid)
            {
                _context.CategoryTools.Add(categoryTool);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(categoryTool);
        }

        // GET: CategoryToolController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryTool = await _context.CategoryTools
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.CategoryToolId == id);
            if (categoryTool == null)
            {
                return NotFound();
            }
            return View(categoryTool);
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryToolToUpdate = await _context.CategoryTools
                .FirstOrDefaultAsync(c => c.CategoryToolId == id);

            if (await TryUpdateModelAsync<CategoryTool>(categoryToolToUpdate,
                "",
                c => c.CategoryToolName))
            {
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException /* ex */)
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
            }
            return View(categoryToolToUpdate);

        }

        // GET: CategoryToolController/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryTool = await _context.CategoryTools
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.CategoryToolId == id);
            if (categoryTool == null)
            {
                return NotFound();
            }
            return View(categoryTool);
        }

        // POST: CategoryToolController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var categoryTool = await _context.CategoryTools.FindAsync(id);
            var tool_of_categorytool =  _context.Tools.Where(x => x.CategoryToolId == id);
            foreach (var item in tool_of_categorytool)
            {
                _context.Tools.Remove(item);
            }
            _context.CategoryTools.Remove(categoryTool);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        private bool CategoryToolExits(int id)
        {
            return _context.CategoryTools.Any(e => e.CategoryToolId == id);
        }
    }
}
