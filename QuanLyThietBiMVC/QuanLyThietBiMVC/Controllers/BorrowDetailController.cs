﻿using Microsoft.AspNetCore.Mvc;
using QuanLyThietBiMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyThietBiMVC.Controllers
{
    [Route("event")]
    public class BorrowDetailController : Controller
    {
        private readonly QuanLyThietBiContext _context;
        public BorrowDetailController(QuanLyThietBiContext context)
        {
            _context = context;
        }

        [Route("")]
        [Route("~/")]

        [Route("index")]
        public IActionResult Index()
        {
            return View();
        }
        [Route("findall")]
        public IActionResult FindAllEvents()
        {
            var events = _context.BorrowDetails.Select(x => new
            {
                id = x.BorrowDetailId,
                description = x.Description,
                startDate = x.TimeBegin,
                endDate = x.TimeEnd,
                color = x.ThemeColor,
                allday = x.IsFullDay,
                userId = x.UserId,
                timelineId = x.TimelineId
            }).ToList();
            return new JsonResult(events);
        }
        [Route("add")]
        [HttpPost]
        public JsonResult AddEvent(BorrowDetail b)
        {
            var status = false;
            var v = _context.BorrowDetails.Where(x => x.BorrowDetailId == b.BorrowDetailId).FirstOrDefault();
            if (b.BorrowDetailId > 0)
            {
                if (v != null)
                {
                    v.Description = b.Description;
                    v.TimeBegin = b.TimeBegin;
                    v.TimeEnd = b.TimeEnd;
                    v.TimelineId = b.TimelineId;
                    v.UserId = b.UserId;
                    v.ThemeColor = b.ThemeColor;
                    v.IsFullDay = b.IsFullDay;
                }
            }
            else
            {
                _context.BorrowDetails.Add(b);
            }
            _context.SaveChanges();
            status = true;
            return new JsonResult(status);
        }
        [Route("delete")]
        [HttpPost]
        public IActionResult DeleteEvent(int id)
        {
            var status = false;
            var v = _context.BorrowDetails.Where(x => x.BorrowDetailId == id).FirstOrDefault();
            if (v != null)
            {
                _context.BorrowDetails.Remove(v);
                _context.SaveChanges();
                status = true;
            }
            return new JsonResult(status);
        }

       
    }

}