﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using QuanLyThietBiMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyThietBiMVC.Controllers
{
    public class UserTakeToolController : Controller
    {
        private readonly QuanLyThietBiContext _context;
        public UserTakeToolController(QuanLyThietBiContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            var usertaketool = _context.UserTakeTools
                .Include(x => x.User)
                .Include(x => x.Tool)
                .ToListAsync();
            return View(await usertaketool);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var course = await _context.UserTakeTools.FirstOrDefaultAsync(m => m.UserTakeToolId == id);
            if (course == null)
            {
                return NotFound();
            }
            return View(course);
        }
        private void PopulateToolsDropDownList(object selectedTool = null)
        {
            var toolsQuery = from d in _context.Tools
                             select d;
            ViewBag.ToolId = new SelectList(toolsQuery.AsNoTracking(), "ToolId", "ToolName", selectedTool);
        }
        private void PopulateUsersDropDownList(object selectedUser = null)
        {
            var usersQuery = from d in _context.Users
                             select d;
            ViewBag.UserId = new SelectList(usersQuery.AsNoTracking(), "UserId", "FirstName", selectedUser);
        }
        public IActionResult Create()
        {
            PopulateToolsDropDownList();
            PopulateUsersDropDownList();
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create([Bind("UserId,ToolId,ReturnOrNot,TimeBorrow,NumberToolTake,TimeReturn")] UserTakeTool usertaketool)
        {
            if (ModelState.IsValid)
            {
                _context.UserTakeTools.Add(usertaketool);
                // tìm ra cái thằng thiết bị muốn cho mượn
                var tool_take_user = _context.Tools.FirstOrDefault(x => x.ToolId == usertaketool.ToolId);
                // tăng cái số lượng thiết bị cho mượn lên
                tool_take_user.NumberOfLendTool += usertaketool.NumberToolTake;
                // giảm số lượng thiết bị còn lại đi
                tool_take_user.NumberOfRestTool = tool_take_user.TotalNumberOfTool - tool_take_user.NumberOfLendTool;
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            PopulateToolsDropDownList(usertaketool.ToolId);
            PopulateUsersDropDownList(usertaketool.UserId);
            ViewData["UserId"] = new SelectList("FirstName" + "LastName", usertaketool.TimeBorrow.ToString(), usertaketool.TimeReturn.ToString(), usertaketool.ReturnOrNot.ToString());
            return View(usertaketool);
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user_take_tool_record = await _context.UserTakeTools.Include(c => c.Tool).Include(d => d.User)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.UserTakeToolId == id);
            if (user_take_tool_record == null)
            {
                return NotFound();
            }

            return View(user_take_tool_record);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user_take_tool_record = await _context.UserTakeTools.FindAsync(id);
            // tìm ra cái thằng thiết bị đã cho mượn
            var tool_repay = _context.Tools.FirstOrDefault(x => x.ToolId == user_take_tool_record.ToolId);
            // giảm đi số thiết bị đã cho mượn
            tool_repay.NumberOfLendTool -= tool_repay.NumberOfLendTool;
            // cập nhật lại cái số thiết bị còn lại
            tool_repay.NumberOfRestTool = tool_repay.TotalNumberOfTool - tool_repay.NumberOfLendTool;
            _context.UserTakeTools.Remove(user_take_tool_record);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        public IActionResult ExportToExcel()
        {
            var data = _context.UserTakeTools
                .Include(x => x.User)
                .Include(x => x.Tool)
                .ToList();
            var stream = new MemoryStream();
            ExcelPackage.LicenseContext = LicenseContext.Commercial;

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;


            using (var package = new ExcelPackage(stream))
            {
                var sheet = package.Workbook.Worksheets.Add("DanhSach");

                // cho dữ liệu vào các sheet
                sheet.Cells[1, 1].Value = "Mã";
                sheet.Cells[1, 2].Value = "Tên người mượn";
                sheet.Cells[1, 3].Value = "Tên Thiết bị mượn";
                sheet.Cells[1, 4].Value = "Số lượng thiết bị mượn";
                sheet.Cells[1, 5].Value = "Đã trả hay chưa";
                sheet.Cells[1, 6].Value = "Ngày mượn";
                sheet.Cells[1, 7].Value = "Ngày trả";
                // và cuối cùng là lưu lại
                int rowIdx = 2;

                foreach (var item in data)
                {
                    sheet.Cells[rowIdx, 1].Value = item.UserTakeToolId;
                    sheet.Cells[rowIdx, 2].Value = item.User.FirstName;
                    sheet.Cells[rowIdx, 3].Value = item.Tool.ToolName;
                    sheet.Cells[rowIdx, 4].Value = item.NumberToolTake;
                    sheet.Cells[rowIdx, 5].Value = item.ReturnOrNot;
                    sheet.Cells[rowIdx, 6].Value = item.TimeBorrow;
                    sheet.Cells[rowIdx, 7].Value = item.TimeReturn;
                    rowIdx++;
                }
                package.Save();
            }
            stream.Position = 0;
                var filename = $"DanhSach_{DateTime.Now.ToString("yyyyMMddHHmmss")}.xlsx";
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",filename);
        }

    }
}
