﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using QuanLyThietBiMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLyThietBiMVC.Controllers
{
    public class ToolController : Controller
    {
        private readonly QuanLyThietBiContext _context;
        public ToolController(QuanLyThietBiContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            var courses = _context.Tools.Include(c=> c.CategoryTool).AsNoTracking();
            return View(await courses.ToListAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var course = await _context.Tools.FirstOrDefaultAsync(m => m.ToolId == id);
            if (course == null)
            {
                return NotFound();
            }
            return View(course);
        }
        private void PopulateCategoryToolsDropDownList(object selectedCategoryTool = null)
        {
            var categoryToolsQuery = from d in _context.CategoryTools
                                   select d;
            ViewBag.CategoryToolId = new SelectList(categoryToolsQuery.AsNoTracking(), "CategoryToolId", "CategoryToolName", selectedCategoryTool);
        }
        public IActionResult Create()
        {
            PopulateCategoryToolsDropDownList();    
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create([Bind("ToolName,Description,UnitMeasure,TotalNumberOfTool,NumberOfLendTool,NumberOfRestTool,CategoryToolId")] Tool tool)
        {
            if (ModelState.IsValid)
            {
                _context.Tools.Add(tool);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            PopulateCategoryToolsDropDownList(tool.CategoryToolId);
            return View(tool);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tool = await _context.Tools
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.ToolId == id);
            if (tool == null)
            {
                return NotFound();
            }
            PopulateCategoryToolsDropDownList(tool.CategoryTool);
            return View(tool);
        }
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toolToUpdate = await _context.Tools
                .FirstOrDefaultAsync(c => c.ToolId == id);

            if (await TryUpdateModelAsync<Tool>(toolToUpdate,
                "",
                c=> c.ToolName, c=> c.Description ,c => c.TotalNumberOfTool, c => c.UnitMeasure, c => c.CategoryTool, c => c.NumberOfRestTool,c =>c.NumberOfLendTool))
            {
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException /* ex */)
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
            }
            PopulateCategoryToolsDropDownList(toolToUpdate.CategoryTool);
            return View(toolToUpdate);

        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var course = await _context.Tools
                .Include(c => c.CategoryTool)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.ToolId == id);
            if (course == null)
            {
                return NotFound();
            }

            return View(course);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tool = await _context.Tools.FindAsync(id);
            _context.Tools.Remove(tool);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        private bool ToolExits(int id)
        {
            return _context.Tools.Any(e => e.ToolId == id);
        }
    }
}
