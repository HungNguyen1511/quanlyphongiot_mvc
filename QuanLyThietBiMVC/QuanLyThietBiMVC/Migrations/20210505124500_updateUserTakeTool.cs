﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLyThietBiMVC.Migrations
{
    public partial class updateUserTakeTool : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tool_CategoryTool_CategoryToolId",
                table: "Tool");

            migrationBuilder.AddColumn<int>(
                name: "NumberToolTake",
                table: "UserTakeTool",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "CategoryToolId",
                table: "Tool",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Tool_CategoryTool_CategoryToolId",
                table: "Tool",
                column: "CategoryToolId",
                principalTable: "CategoryTool",
                principalColumn: "CategoryToolId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tool_CategoryTool_CategoryToolId",
                table: "Tool");

            migrationBuilder.DropColumn(
                name: "NumberToolTake",
                table: "UserTakeTool");

            migrationBuilder.AlterColumn<int>(
                name: "CategoryToolId",
                table: "Tool",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Tool_CategoryTool_CategoryToolId",
                table: "Tool",
                column: "CategoryToolId",
                principalTable: "CategoryTool",
                principalColumn: "CategoryToolId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
