﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLyThietBiMVC.Migrations
{
    public partial class UpdateBorrowDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsFullDay",
                table: "BorrowDetail",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ThemeColor",
                table: "BorrowDetail",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsFullDay",
                table: "BorrowDetail");

            migrationBuilder.DropColumn(
                name: "ThemeColor",
                table: "BorrowDetail");
        }
    }
}
