﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyThietBiMVC.Models
{
    public class EntityBases
    {
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? DeletedBy { get; set; }
    }
}
