﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyThietBiMVC.Models
{
    public class CategoryTool : EntityBases
    {
        [Key]
        public int CategoryToolId { set; get; }

        public string CategoryToolName { set; get; }

        public ICollection<Tool> Tools { get; set; }
    }
}
