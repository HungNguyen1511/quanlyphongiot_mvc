﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace QuanLyThietBiMVC.Models
{
    public class UserTakeTool: EntityBases
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserTakeToolId { set; get; }
        public int UserId { set; get; }
        public int ToolId { set; get; }
        [ForeignKey("ToolId")]
        public virtual Tool Tool { set; get; }
        [ForeignKey("UserId")]
        public virtual User User { set; get; }
        public int NumberToolTake { set; get; }
        public bool ReturnOrNot { set; get; }
        public DateTime TimeBorrow { set; get; }
        public DateTime TimeReturn { set; get; }
    }
}
