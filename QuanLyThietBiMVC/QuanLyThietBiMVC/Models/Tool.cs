﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace QuanLyThietBiMVC.Models
{
    public class Tool : EntityBases
    {
        [Key]
        public int ToolId { set; get; }
        public string ToolName { set; get;}
        public string Description { set; get; }
        public string UnitMeasure { set; get; }
        public int TotalNumberOfTool { set; get; }
        public int NumberOfLendTool { set; get; }
        public int NumberOfRestTool { set; get; }
        [ForeignKey("CategoryToolId")]
        public int CategoryToolId { set; get; }
        public virtual CategoryTool CategoryTool { set; get; }
        public ICollection<UserTakeTool> UserTakeTools { get; set; }
    }
}
