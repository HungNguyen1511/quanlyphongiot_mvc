﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLyThietBiMVC.Models
{
    public class QuanLyThietBiContext : DbContext
    {
        public QuanLyThietBiContext(DbContextOptions options)
            : base(options)
        {
        }
        public DbSet<CategoryTool> CategoryTools { get; set; }
        public DbSet<History> Historys { get; set; }
        public DbSet<Timeline> Timelines { get; set; }
        public DbSet<Tool> Tools { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserTakeTool> UserTakeTools { get; set; }

        public DbSet<BorrowDetail> BorrowDetails { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CategoryTool>().ToTable("CategoryTool");
            modelBuilder.Entity<History>().ToTable("History");
            modelBuilder.Entity<Timeline>().ToTable("Timeline");
            modelBuilder.Entity<Tool>().ToTable("Tool");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<UserTakeTool>().ToTable("UserTakeTool");
            modelBuilder.Entity<BorrowDetail>().ToTable("BorrowDetail");
        }


    }
}
