﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace QuanLyThietBiMVC.Models
{
    public class BorrowDetail : EntityBases
    {
        [Key]
        public int BorrowDetailId { set; get; }
        public DateTime TimeBegin { set; get; }
        public DateTime TimeEnd { set; get; }
        public string Description { set; get; }
        [ForeignKey("TimelineId")]
        public virtual int TimelineId {set;get;}
        [ForeignKey("UserId")]
        public virtual int UserId { set; get; }
        public string ThemeColor { set; get; }
        public bool IsFullDay { set; get; }
        


    }
}
