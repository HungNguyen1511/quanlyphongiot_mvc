﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyThietBiMVC.Models
{
   
    public class Timeline : EntityBases
    {
        [Key]
        public int TimeLineId { set; get; }

        public string Day { set; get; }

        public ICollection<BorrowDetail> BorrowDetails { get; set; }
    }
}
