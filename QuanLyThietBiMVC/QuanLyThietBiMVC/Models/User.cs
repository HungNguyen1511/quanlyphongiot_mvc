﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLyThietBiMVC.Models
{
    public class User : EntityBases
    {
        [Key]
        public int UserId { set; get; }
        [MaxLength(20)]
        public string FirstName { set; get; }
        [MaxLength(20)]
        public string LastName { set; get; }
        [MaxLength(10)]
        public string CodeCard { set; get; }
        [MaxLength(20)]
        public string Address { set; get; }
        [MaxLength(10)]
        public string PhoneNumber { set; get; }

        public ICollection<BorrowDetail> BorrowDetails { set; get; }
        public ICollection<UserTakeTool> UserTakeTools { get; set; }
    }
}
